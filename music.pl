#!/usr/bin/perl

my $ffmt = 'ff%s -v 8 -f lavfi -i "sine=frequency=%.2f:sample_rate=48000:duration=%s" -c:a pcm_s16le -autoexit -nodisp';
$ffmt = 'ff%s -v 8 -f lavfi -i "sine=frequency=%.2f:sample_rate=48000:duration=%s" %s "%s"';


#do ♮♭ re ♮♭ mi fa ♮♭ sol ♮♭ la ♮♭ ti
#D R  MF S L  T
# d rm  f s lt 

# Schumann [*](https://en.wikipedia.org/wiki/Schumann_resonances):
# 4.11
my $fS = 7.83; # Hz (fundamental),
# 14.3,
# 20.8,
# 27.3,
# 33.8

#C0,D0♭,D0,E0♭,E0,F0,G0♭,G0,A0♭,A0,B0♭,B0
my @scale = qw(do re mi fa sol la ti);
my @scale7 = qw(C D E F G A B);
my @steps7 = qw( 2 2 1 2 2 2 1);
my @scale12 = qw(C C♮/D♭ D D♮/E♭ E F F♮/G♭ G G♮/A♭ A A♮/B♭ B);
my $alphab = 'A2BC4D5EF7G1'; # chromatic scale
# fn = f0 * a^n
# Wn = c/fn
# a = 2 ^ (1/12)

my $c = 345; # m/s
my $a = exp (log(2)/12);
printf "a: %s\n",$a;

my $A4 = shift || 432;
my $C0 = $A4 / ($a**(4*12+9));
if ($A4 == 421) {
  $C0 = 2 * $fS;
  $A4 = $C0 * ($a**(4*12+9));
}
printf "fSCH = %.4fHz\n",$C0/2;
printf "f(C0) = %.4fHz\n",$C0;
printf "f(A4) =  %.2fHz\n",$A4;
my $A5 = 2 * $A4;

#C middle =  A4 - 12 + 3
my $fC = $A4 / ($a**9);
my $oct = 4;
if (! -e 'scale/A4.wav') {
for my $n (-5*12, -12 .. 24) {
  my $o = int ($oct + $n/12);
  my $fn = $fC * ($a**$n);
  my $q = $a**($n-9);
  my $note = $scale12[$n % 12];
  $note = (split('/',$note))[int(rand(2))] if ($note =~ m'/');
  my $notep = $note;
  $notep =~ s/(.)/\1$o/;
  my $notef = sprintf'scale/%s.wav',$notep;
  $options = '-y -c:a pcm_s16le';
  my $cmd = sprintf $ffmt,'mpeg',$fn,0.5,$options,$notef;
  #system $cmd;
  my $cmd = sprintf $ffmt,'mpeg',$fn,0.2,'-y',sprintf('f%d.mp3',$n);
  system "$cmd 1>/dev/null 2>&1";
  if (length($note) == 1) {
     printf "f%d(%3s) = %.3f Hz = (%.1f * %.4f)\n",$n,$notep,$fn,$A4,$q;
     my $options = '-autoexit'; 
     my $cmd = sprintf $ffmt,'play',$fn,0.5,$options,'-nodisp';
     #system $cmd;
  }
}

}

while (1) {
  my $ans = <>;
  chomp($ans);
  my $phrase = lc $ans;
  $phrase =~ y/a-z/./cs;
  printf "phrase: %s (%uc)\n",$phrase,length($phrase);
  my $phbin = &decode_basea($phrase,'.abcdefghijklmnopqrstuvwxyz');
  printf "phrase: f%s (%uc)\n",unpack('H*',$phbin),length($phbin);
  my $ph12 = &encode_base12($phbin);
  printf "ph12: %s (%uc)\n",$ph12,length($ph12);
  my $ph13 = &encode_basea($phbin,' 0123456789XE');
  printf "ph13: '%s' (%uc)\n",$ph13,length($ph13);
  
  my $zero = 0;
  my @phrase = &encode_baser($phbin,25);
  my @files = ();
  foreach my $c (@phrase) {
     my $n = ($c - 1) - 6;
     my $o = int ($oct + $n/12);
     my $fn = ($c != $zero) ? $fC * ($a**$n) : $fS * (2**$o);
     my $note = ($c != $zero) ? $scale12[$n % 12] : 'fS';
     $note = (split('/',$note))[int(rand(2))] if ($note =~ m'/');
     my $notep = $note;
     $notep =~ s/(.)/\1$o/;
     my $duration = 0.2 + sqrt(rand(1/3));
        $duration /= 2;
     if ($c != $zero) {
        printf qq'f%d: %s %.3fHz %ums\n',$n,$notep,$fn,$duration*1000;
     } else {
        printf qq'fS%d: %s %.3fHz %ums\n',$c,$notep,$fn,$duration*1000;
     }
     my $notef = sprintf'scale/%s.wav',$notep;
     my $options = '-autoexit'; 
     my $cmd = sprintf $ffmt,'play',$fn,$duration,$options,'-nodisp';
     system "$cmd 1>/dev/null 2>&1";
     my $fnf = ($c != 24) ? sprintf('freqs/f%d.wav',$n) : $notef;
     my $cmd = sprintf $ffmt,'mpeg',$fn,$duration,'-y',$fnf;
     system "$cmd 1>/dev/null 2>&1";
     push @files,$fnf;
  }
  my $outf = sprintf('%s-%.0f.mp3',$phrase,$A4);
  my $cmd = join' ','sox',@files,'out.wav';
  system $cmd;
     $cmd = sprintf'ffmpeg -y -v 8 -i out.wav %s',$outf;
  system $cmd;
  $cmd = sprintf 'ffplay -v 8 "%s" -autoexit -loop 5',$outf;
  system $cmd;
  print ".\n";
  
}

exit $?;

# see [*](https://pages.mtu.edu/~suits/notefreq432.html)

sub encode_base12 {
  use Math::BigInt;
  my $bin = join'',@_;
  my $n = Math::BigInt->from_bytes($bin);
  my $e = '';
  while ($n->bcmp(0) == +1)  {
    my $c = Math::BigInt->new();
    ($n,$c) = $n->bdiv(12);
    $e .= chr(0x30 + $c->numify); # 0x30: 0 included
  }
  $e =~ y/0-9:;/0-9XE/;
  return scalar reverse $e;
}


sub encode_baser {
  use Math::BigInt;
  my ($d,$radix) = @_;
  my $n = Math::BigInt->from_bytes($d);
  my @e = ();
  while ($n->bcmp(0) == +1)  {
    my $c = Math::BigInt->new();
    ($n,$c) = $n->bdiv($radix);
    push @e, $c->numify;
  }
  return reverse @e;
}

sub encode_basea { # w. a passed alphabet
  use Math::BigInt;
  my ($data,$alphab) = @_;
  $alphab = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ -+.$%' unless $alphab; # barcode 3 to 9
  my $radix = length($alphab);
  my $mod = Math::BigInt->new($radix);
  #printf "mod: %s, lastc: %s\n",$mod,substr($alphab,$mod,1);
  my $h = '0x'.unpack('H*',$data);
  my $n = Math::BigInt->from_hex($h);
  my $e = '';
  while ($n->bcmp(0) == +1)  {
    my $c = Math::BigInt->new();
    ($n,$c) = $n->bdiv($mod);
    $e .= substr($alphab,$c->numify,1);
  }
  return scalar reverse $e;
}

sub decode_basea { # passing an alphabet ...
  use Math::BigInt;
  my ($s,$alphab) = @_;
  $alphab = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ -+.$%' unless $alphab; # barcode 3 to 9
  my $radix = Math::BigInt->new(length($alphab));
  my $n = Math::BigInt->new(0);
  my $j = Math::BigInt->new(1);
  while($s ne '') {
    my $c = substr($s,-1,1,''); # consume chr from the end !
    my $i = index($alphab,$c);
    return '' if ($i < 0);
    my $w = $j->copy();
    $w->bmul($i);
    $n->badd($w);
    $j->bmul($radix);
  }
  my $h = $n->as_hex();
  # byte alignment ...
  my $d = int( (length($h)+1-2)/2 ) * 2;
  $h = substr('0' x $d . substr($h,2),-$d);
  return pack('H*',$h);
}
# ---------------------------



1;






